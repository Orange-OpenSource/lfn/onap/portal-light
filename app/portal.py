# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

""" The app """

import yaml
from flask import (Flask,
                   render_template,
                   redirect,
                   request,
                   make_response,
                   session,
                   abort)
from flask_login import (LoginManager,
                         login_user,
                         logout_user)
from loguru import logger


#
# encrypt and decryptPKC are translatted from portal java code
# cf portal-sdk/ecomp-sdk/epsdk-fw/src/main/java/org/onap/portalsdk/core/onboarding/util/CipherUtil.java
#

from base64 import b64decode
from base64 import b64encode
from Crypto import Random
from Crypto.Cipher import AES

BLOCK_SIZE = 16

def pad(string):
    '''pad string'''
    return string + (BLOCK_SIZE - len(string) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(string) % BLOCK_SIZE)


def unpad(string):
    '''unpas string'''
    return string[:-ord(string[len(string) - 1:])]


def encrypt(string, key):
    '''
    Basic encryption function as implemented in ONAP portal (deprecated)
    using AES in ECB mode
    '''
    cipher = AES.new(b64decode(key), AES.MODE_ECB)
    return b64encode(cipher.encrypt(pad(string)))


def decryptPKC(string, key):
    '''
    Decrypts the text using a secret key, as done in ONAP portal,
    using AES in CBC mode
    '''
    ibytes = b64decode(string)
    ivect = ibytes[:16]
    cipher = AES.new(b64decode(key), AES.MODE_CBC, ivect)
    return unpad(cipher.decrypt(ibytes[16:])).decode('utf8')


# utils we dont need in the app
def decrypt(string, key):
    '''
    Basic decryption function as implemented in ONAP portal (deprecated)
    using AES in ECB mode
    '''
    cipher = AES.new(b64decode(key), AES.MODE_ECB)
    return unpad(cipher.decrypt(b64decode(string)))


def encryptPKC(string, key):
    '''
    Encrypts the text using a secret key, as done in ONAP portal,
    using AES in CBC mode
    '''
    ivect = Random.new().read(AES.block_size)
    cipher = AES.new(b64decode(key), AES.MODE_CBC, ivect)
    return b64encode(ivect + cipher.encrypt(pad(string)))


#
# Config
#
class Config:
    '''
    Helper class to get values from configuration
    '''
    def __init__(self, filename=None, params=None):
        self.params = {}
        if filename:
            with open(filename, 'r') as stream:
                self.params = yaml.load(stream, Loader=yaml.FullLoader)
        elif params:
            self.params = params

    def __getitem__(self, key):
        params = self.params
        for k in key.split('.'):
            if k in params:
                params = params[k]
            else:
                return None
        if params != self.params:
            return params

    def __contains__(self, key):
        params = self.params
        if key:
            for k in key.split('.'):
                if params and k in params:
                    params = params[k]
                else:
                    return False
        return True

    def get(self, key, default=None):
        if key in self:
            return self[key]
        return default

    def __setitem__(self, key, value):
        path = key.split('.')
        params = self.params
        for k in path[:-1]:
            if k not in params:
                params[k] = {}
            params = params[k]
        params[path[-1]] = value

    def set(self, key, value):
        self[key] = value

    def __delitem__(self, key):
        if key:
            params = self.params
            path = key.split('.')
            for k in path[:-1]:
                if params and k in params:
                    params = params[k]
                else:
                    return
            del params[path[-1]]

    def setdefault(self, key, value):
        if key not in self:
            self.set(key, value)


#
# App
#

from directory import BasicDirectory

app = Flask(__name__)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = ''


@login_manager.user_loader
def load_user(uid):
    "User loader function for Flask Login-Manager"
    logger.debug('Flask_Login::load_user({})'.format(uid))
    uuid, username = uid.split(':', 1)
    directory = next((d for d in directories if d.uuid() == uuid), None)
    if directory:
        return directory.find(username)
    return None


def find_user(username):
    "User lookup in all directories, by priority, using its username"
    for d in directories:
        user = d.find(username)
        if user:
            return user
    return None


def define_routes(routemap):
    @app.route(routemap('/ping'))
    def ping():
        return('', 204)

    @app.route(routemap('/'))
    @app.route(routemap('/login.htm'))
    def root():
        return render_template('main.j2', config=config)

    rootdir = '/'
    if routemap(rootdir) != rootdir:
        @app.route('/')
        def redirectroot():
            return redirect(routemap('/'), code=302)

    @app.route(routemap('/home'))
    def home():
        return redirect(routemap('/'), code=302)

    @app.route(routemap('/login'), methods=['GET', 'POST'])
    def login():
        if request.method == 'POST':
            credentials = request.json
            username = credentials.get("loginId")
            password = credentials.get("password")
            logger.debug("Login(username=<{}>)".format(username))
            user = find_user(username)
            if user:
                try:
                    if user.authenticate(password):
                        response = make_response('{"success": "success"}')
                        response.set_cookie('USER_ID',
                                            username,
                                            path='/',
                                            domain=domain)
                        key = config['onap.encryption-key']
                        response.set_cookie('UserId',
                                            encrypt(username, key),
                                            path='/',
                                            domain=domain)
                        login_user(user)
                        return response
                    else:
                        logger.info("Authentication failed for user=<{}/{}>",
                                    username,
                                    user.dirname())
                except Exception as exc:
                    logger.info("Authentication failed for user=<{}/{}>: {}",
                                username,
                                user.dirname(),
                                exc)
                abort(401)
            else:
                logger.info("Authentication failed for user=<{}>: not found",
                            username)
                abort(404)
        return redirect(routemap('/'))

    @app.route(routemap('/logout'))
    def logout():
        logger.debug('Logout')
        logout_user()
        response = make_response(render_template('main.j2'))
        logger.debug('Removing cookies ...')
        response.delete_cookie('UserId', path=routemap('/'), domain=domain)
        response.delete_cookie('USER_ID', path=routemap('/'), domain=domain)
        return response


def mkdir(config, params):
    '''
    Create a user directory
    '''
    directory = None
    pri = params.get('priority', -1)
    if pri >= 0:
        typeof = params.get('type')
        if typeof == 'basic':
            key = config['onap.encryption-key']
            directory = BasicDirectory(params.get('name'),
                                       Config(params=params),
                                       encrypt=lambda x: encryptPKC(x, key),
                                       decrypt=lambda x: decryptPKC(x, key))
    return directory, pri


def toolbox(config):
    "Use the app as a toolbox if requested"
    func = None
    if config.get('toolbox.pkc'):
        pkc = True
    if config.get('toolbox.encrypt'):
        if pkc:
            func = encryptPKC
        else:
            func = encrypt
    elif config.get('toolbox.decrypt'):
        if pkc:
            func = decryptPKC
        else:
            func = decrypt
    if func:
        print(func(sys.stdin.read(), config['onap.encryption-key']))
        exit(0)


if __name__ == '__main__':
    import os, sys, getopt

    config = None
    filename = os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                            'portal.yaml')
    opts, args = getopt.getopt(sys.argv[1:],
                               "f:k:",
                               ["encrypt", "decrypt", "pkc", "key=", "value="])
    for opt, arg in opts:
        if opt == '-f':
            filename = arg
    filename = os.path.abspath(filename)
    if not os.path.exists(filename):
        filename = None

    logger.info("Using configuration file=<{}>".format(filename))
    config = Config(filename)

    # Toolbox helper
    for opt, arg in opts:
        if opt == '--encrypt':
            config['toolbox.encrypt'] = True
        elif opt == '--decrypt':
            config['toolbox.decrypt'] = True
        elif opt == '--pkc':
            config['toolbox.pkc'] = True
        elif opt in ('-k', '--key'):
            config['onap.encryption-key'] = arg
        elif opt == '--value':
            name, value = arg.split('=', 1)
            config[name] = value

    if 'toolbox' in config:
        toolbox(config)
        exit(0)

    # Logging
    logger.remove()
    verbosity = config.get('app.verbosity', 'INFO')
    logger.add(sys.stdout, level=verbosity)

    # init directories (not idle and sorted by priority)
    directories = list(filter(lambda d: d.config.get('state') != 'idle',
                              map(lambda it: it[0],
                                  sorted(map(lambda it: mkdir(config, it),
                                             config.get('directories', ())),
                                         key=lambda d: d[1]))))
    for directory in directories:
        logger.info("directory#<{}:{}> starting sync",
                    directory.name,
                    directory.uuid())
        directory.synchronize()
        logger.info("directory#<{}:{}> sync done",
                    directory.name,
                    directory.uuid())

    app.config['SECRET_KEY'] = config['app.secret-key']
    approot = config.get('app.root', '').strip()
    if approot:
        approot = "/{}".format(approot.strip('/'))
    define_routes(lambda path: "{}{}".format(approot, path))

    # set domain for cookies
    domain = config.get('app.domain', None)
    if domain is None:
        default = 'simpledemo.onap.org'
        logger.warning('domain not set, using default=simpledemo.onap.org')
        domain = default

    app.run(host=config.get('www.host', "0.0.0.0"),
            port=config.get('www.port', 1234),
            debug=True if verbosity == 'DEBUG' else False)
