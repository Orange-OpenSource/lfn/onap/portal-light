# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

""" User directories """

from flask_login import UserMixin
from loguru import logger


class User(UserMixin):
    """
    Base Directory User
    """
    def __init__(self, name, properties=None, directory=None):
        self.id = name
        self.properties = properties if properties else {}
        self.properties['username'] = self.id
        self.directory = directory
        self.membership = []

    def __str__(self):
        return ("User#<name=<{}>, "
                "directory=<{}>, "
                "properties={}>").format(self.id,
                                         self.directory.name,
                                         self.properties)

    def get_id(self):
        "Get a user id, as string. Needed for Flask login manager"
        uuid = self.directory.uuid() if self.directory else None
        return "{}:{}".format(uuid, self.username())

    def authenticate(self, password):
        "Autenticate against the supplied password"
        if self.directory:
            return self.directory.authenticate(self, password)
        else:
            raise Exception("Unbound user <{}>".format(self.username()))

    def username(self):
        return self.id

    def displayname(self):
        dn = self.get('displayname')
        if not dn:
            dn = "{} {}".format(self.get('firstname', ''),
                                self.get('lasttname', '')).strip()
        if not dn:
            dn = self.username()
        return dn

    def groups(self):
        "List of groups the user belongs to"
        return self.membership

    def __contains__(self, key):
        return (key in self.properties)

    def __getitem__(self, key):
        return self.properties[key]

    def get(self, key, default=None):
        "Get a property value with a default value if unset"
        return self.properties.get(key, default)

    def dirname(self):
        return self.directory.dirname() if self.directory else ""

class Group:
    """
    Base Group
    """

    def __init__(self, name):
        self.name = name

    def groupname(self):
        return self.name


class Directory:
    """
    Base directory
    """

    def __init__(self, name):
        self.name = name

    def uuid(self):
        return "{}".format(hash(self))

    def dirname(self):
        return self.name

    def synchronize(self):
        "Synchronize the directory using its current config"
        pass

    def find(self, username):
        "Find a user with the specified username in the current directory"
        pass

    def authenticate(self, user, password):
        "Authenticate the specified user. Raise an Exception on failure"
        pass

    def register(self, user, password):
        "Register the user using the specified user into the current dir"
        pass

    def unregister(self, user):
        "Unregister the specified user from the current dir"
        pass


#
# Basic Directory
#

class BasicDirectory(Directory):
    """
    Basic directory implementation using a config object (that may be
    built against a plain text yaml file) as input
    """

    ident = lambda x: x

    ###
    def __init__(self, name, config, encrypt=ident, decrypt=ident):
        Directory.__init__(self, name)
        self.users = {}
        self.encrypt = encrypt
        self.decrypt = decrypt
        self.config = config

    #
    def __str__(self):
        return str(self.users)

    #
    def synchronize(self):
        "Synchronize the directory using its current config"

        logger.debug('Directory<{}>::synchronize()', self.name)
        self.users = {}
        for properties in self.config['users']:
            name = properties['name']
            user = User(name, properties, self)
            if name not in self.users:
                self.users[name] = user
            else:
                logger.warning("Multiple occurences of user <{}> while"
                                " synchronizing directory <{}>."
                                " Ignoring",
                               name,
                               self.name)

    #
    def find(self, username):
        "Find a user with the specified username in the current directory"

        logger.debug("Directory<{}>::find(username=<{}>)",
                     self.name,
                     username)
        if username in self.users:
            user = self.users[username]
            properties = dict(user.properties)
            properties.pop('password', None)
            return User(username, properties=properties, directory=self)

    #
    def authenticate(self, user, password):
        "Authenticate the specified user. Raise an Exception on failure"

        try:
            if user.directory == self:
                p = self.users[user.username()]['password']
                if (self.encrypt and p == self.encrypt(password)) or \
                   (self.decrypt and self.decrypt(p) == password):
                    logger.debug("Directory<{}>::autenticate({}) ok",
                                 self.name,
                                 user.username())
                    return True
                raise Exception('Password mismatch')
            raise Exception('Unbound user')
        except Exception as ex:
            logger.debug("Directory<{}>::autenticate({}) failure={}",
                         self.name,
                         user.username(),
                         ex)
            raise ex

    #
    def register(self, user, password):
        "Register the user using the specified user into the current dir"

        username = user.username()
        if username not in self.users:
            properties = dict(user.properties)
            properties['password'] = self.encrypt(password)
            user.directory = self
            self.users[username] = properties
        else:
            raise Exception("User allready exist")
        return True

    #
    def unregister(self, user):
        "Unregister the specified user from the current dir"

        if user.directory == self:
            username = user.username()
            if username in self.users:
                del self.users[username]

