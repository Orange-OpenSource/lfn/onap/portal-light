FROM python:3.7-alpine
MAINTAINER Nicolas Edel <nicolas.edel@orange.com>


RUN apk --no-cache add --virtual build build-base libffi-dev openssl-dev && \
    python3 -m pip install pycrypto flask flask_login pyyaml loguru && \
    apk --no-cache del build

USER guest
COPY app/ /app
COPY etc/ /etc/portal

CMD ["/usr/bin/env", "python3", "/app/portal.py", "-f", "/etc/portal/portal.yaml"]

