
The portal-light aims at providing the basic functions of the ONAP portal using
a minimal footprint. It is provided as a basic Flask application using the builtin
WSGI server. As stated at runtime:
```
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
```



##  Running the portal app

### standalone mode

From a shell:
```
# python3 app/portal.py -f etc/portal.yaml
```

### docker mode

- build the container:
```
# docker build -t <tag> .
```

When a proxy has to be used, modify the command:
```
# docker build --build-arg http_proxy=http://<yourproxy> --build-arg https_proxy=http://<yourproxy> -t <tag> .
```

And then either use the local image or push it to a registry


##  User managment

The portal just do user authentication as it is done in the real portal app.
By default the users are read from an internal directory defined in the
portal configuration file. The default configuration file contains the same
default users as the portal does, using the same default passwords (ie 
demo123456! for all users).

If you want to change the passwords, you may generate new ones using the
toolbox exposed by the app. Ex:

```
# printf "mypassword" | python3 app/portal.py -f etc/portal.yaml --encrypt --pkc
; Using configuration file=<xxx/etc/portal.yaml>
gekG93UgHq9qlIUaA6fWn5Zno+U1QqoqjMeVZggrLGw=
```

If you want to encrypt or decrypt using other encryption key, you may just use
the --key option. This has the same effect as overriding the onap.encryption-key
value from the config file.
Ex:
```
# printf "mypassword" | python app/portal.py --encrypt --pkc --key AGLDdG4D04BKm2IxIWEr8o==
or
# printf "mypassword" | python app/portal.py --encrypt --pkc --value onap.encryption-key=AGLDdG4D04BKm2IxIWEr8o==
```

You can use the app the same way to decrypt previously encrypted strings:
```
# printf <your-encrypted-string> | python app/portal.py --decrypt --pkc --key AGLDdG4D04BKm2IxIWEr8o==
```

The default configuration file use the same encryption key used in the current
ONAP portal.


##  Accessing the ONAP applications

The applications are declared in the configuration file, and may be extended
using a --value option.
Each application consist in a pair of (name, URL).
As for the original portal app, the applications are exposed using iframes.


##  Portal URL

In kubernetes mode, the portal is exposed on port 30089. This is a kubernetes
nodePort to make it easy.
The default URL is the same the original portal is using: /ONAPPORTAL/login.htm
You may change the /ONAPPORTAL part by changing the app.root value in the config
file. When using Helm, this value is set to the www.root value from the
values.yaml file

### Crypto

The crypto functions are those from the portal-sdk module, rewritten in python
using the PyCrypto module. The functions (en|de)crypt[PKC] names are the same
as in the java code for clarity

_cf_ portal-sdk/ecomp-sdk/epsdk-fw/src/main/java/org/onap/portalsdk/core/onboarding/util/CipherUtil.java

### Javascript

Most a the javascript code has been taken from the original portal-app code
and slightly modified

